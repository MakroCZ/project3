import tkinter as tk
from PIL import Image, ImageTk
from tkinter.filedialog import askopenfilename

# Původní pokus řešení s grafickým rozhraním, nepoužívám
class MazeSolver(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        self.grid()
        self.create_widgets()

    def create_widgets(self):
        self.button_choose_maze = tk.Button(self, text="Choose maze", command=self.choose_maze)
        self.button_choose_maze.grid(row=0, column=0)

    def choose_maze(self):
        file_path = tk.filedialog.askopenfilename()
        self.show_maze(file_path)

#http://www.swharden.com/wp/2010-03-03-viewing-large-images-with-scrollbars-using-python-tk-and-pil/
    def show_maze(self, file_path):
        img = Image.open(file_path)
        photo_img = ImageTk.PhotoImage(img)

        canv_image = tk.Canvas(self)
        canv_image.config(width=500, height=500)
        canv_image.config(highlightthickness=0)

        sbarV = tk.Scrollbar(self, orient="vertical")
        sbarH = tk.Scrollbar(self, orient="horizontal")

        canv_image.config(yscrollcommand=sbarV)
        canv_image.config(xscrollcommand=sbarH)
        canv_image.grid(row=1, column=0)

        width, height = img.size
        canv_image.config(scrollregion=(0,0,width,height))
        self.imgtag = canv_image.create_image(0,0,anchor="nw", image=photo_img)


app = MazeSolver()
app.master.title("Maze Solver")
app.mainloop()
import sys
from PIL import Image
from collections import deque

from MyPixel import MyPixel


class MazeSolverNG:

    def __init__(self):
        self.start_found = False
        self.end_found = False

        self.COLOR_PATH = (255, 0, 0)
        self.COLOR_START = (0, 255, 0)
        self.COLOR_FINISH = (0, 0, 255)
        self.COLOR_WALL = (0, 0, 0)
        self.COLOR_SPACE = (255, 255, 255)

        #self.load_image(sys.argv[1])
        self.load_image("Data/Maze.png")
        self.binarize_image()

    def load_image(self, file_path):
        """
        Load image, save dimensions
        """
        self.img = Image.open(file_path)
        self.width, self.height = self.img.size

    def binarize_image(self):
        """
        Maze analyze, find start, finish, binarize maze
        """
        print("Start image analyze")
        maze = {}
        for x in range(self.width):
            for y in range(self.height):
                r, g, b = self.img.getpixel((x, y))
                if (r, g, b) == self.COLOR_START:
                    self.start = (x, y)
                    r, g, b = self.COLOR_SPACE
                    self.img.putpixel((x, y), self.COLOR_SPACE)
                if (r, g, b) == self.COLOR_FINISH:
                    self.end = (x, y)
                    r, g, b = self.COLOR_SPACE
                    self.img.putpixel((x, y), self.COLOR_SPACE)
                if r+g+b < 500:
                    wall = True
                    self.img.putpixel((x, y), self.COLOR_WALL)
                else:
                    wall = False
                    self.img.putpixel((x, y), self.COLOR_SPACE)
                maze[(x, y)] = MyPixel(wall)
        print("Saving binary image")
        self.img.save("Data/binary.png")
        self.calculate_price(maze)
        self.find_path(maze)

    def calculate_price(self, maze):
        for pixel in maze.values():
            if not pixel.wall:

                for dx, dy in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                    if (x + dx, y + dy) in maze:
                        new_pixel = maze.get((x + dx, y + dy))

        distance = 0


    def find_path(self, maze):
        """
        Find all possible path from start to finish through unprocessed pixels, using BFS
        """
        maze = maze
        storage = deque()
        storage.append(self.start)
        print("Start finding path")

        while len(storage) > 0:
            x, y = storage.popleft()
            pixel = maze.get((x, y))

            if not pixel.processed:
                for dx, dy in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                    if (x + dx, y + dy) in maze:
                        new_pixel = maze.get((x + dx, y + dy))
                        if not new_pixel.wall and not new_pixel.processed:
                            storage.append((x + dx, y + dy))
                            new_pixel.parent_pixel = (x, y)
            pixel.processed = True

        self.paint_path(maze)

    def paint_path(self, maze):
        """
        Paint the path
        """
        coordination = self.end
        print("Painting path")
        self.img.putpixel(coordination, self.COLOR_PATH)
        while not coordination == self.start:
            pixel = maze.get(coordination)
            coordination = pixel.parent_pixel
            self.img.putpixel(coordination, self.COLOR_PATH)

        self.img.save("Data/Path.png")
        print("Maze solved")

app = MazeSolverNG()
